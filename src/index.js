import React from 'react';
import ReactDOM from 'react-dom';
import Game from './js/components/game.js';

// style
import './styles/pages/index.scss';
import './styles/pages/search-result.scss';


ReactDOM.render(
	
  <Game />,
  document.getElementById('root')
);

import BoardModel from '../models/board.model.js';

let boardModel = new BoardModel();

test('start num to be 10', () => {
  expect(boardModel.data.num).toBe(10);
});

test('calculate sum', () => {
	expect(boardModel.calTotal()).toBe(45);
});

test('init squares', () => {
	expect(boardModel.data.squares.length).toBe(boardModel.data.num);
});

test('when model instantiated, num of movies should be 0', () => {
	expect(boardModel.data.movies.length).toBe(0);
});

test('fetch movies, movies has to be more than 0', () => {
	let promise = boardModel.fetchBoardDetail();
	promise.then(function() {
		expect(boardModel.data.movies.length).toBeGreaterThan(0);
	});
});


//view component
import React from 'react';
import Board from './board.js';
import Movies from './movies.js';
import ScoreBoard from './score-board.js';
import TicketReader from './ticket-reader.js';
// model
import BoardModel from '../models/board.model.js';

// services
import brain from '../services/brain.js';


var QRCode = require('qrcode.react');


export default class Game extends React.Component {
  constructor (){
    super();
    this.boardModel = new BoardModel();
    this.boardModel.calTotal();
    this.state = this.boardModel;
    this.onInit();
  }

  onInit() {
    brain.on('hoversquare', this.onHoverSquare, this);
  }

  onHoverSquare() {
    this.boardModel.calTotal();
    brain.trigger('updatetotal', [this.state.data.sum]);
    this.boardModel.fetchBoardDetail();
  };

  render() {
    return (
      <div className="game">
     
       
        <div className="game-movies">
          <Movies boardModel={this.boardModel}/>
        </div>

        <div className="game-board">
          <Board boardModel={this.boardModel}/>
        </div>

        <div className="game-info">
          <div>{/* status */}</div>
          <ol>{/* TODO */}</ol>
          <ScoreBoard sum={this.state.data.sum}/>
        </div>
      </div>
    );
  }
}
import React from 'react';
import Square from './square.js';

export default class Board extends React.Component {
  constructor(props){
    super();

    this.boardModel = props.boardModel;
    // this.state = this.boardModel;
    this.state = this.boardModel.data;
  
    this.boardModel.addScope({
      name: 'board',
      scope: this
    });
  }

  renderSquare(i) {
    return <Square foo="hello" value={i}/>;
  }

  render() {
    let squares = this.state.squares.map((s, i)=>{
        return <Square key={i} value={s} />;
    })
    return <ul>{squares}</ul>
  }
}




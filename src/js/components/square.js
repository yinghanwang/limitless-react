import React from 'react';
import brain from  '../services/brain.js';

export default class Square extends React.Component {
    constructor(props) {
        super(props);
        
        var me = this;

        // interface section
        this.model = props.value;
        this.model.setScope({
            name: 'square',
            scope: me
        });
        this.state = this.model;

    }

    onHoverSquare() {
        this.model.doubleValue();
        brain.trigger('hoversquare');
    }

    onClickSquare() {
        this.model.resetBackToOne();
    }

    render() {
        return (
            <button className="square" onMouseOver={()=>this.onHoverSquare()} onClick={()=>this.onClickSquare()}>
                {this.state.getValue()}
            </button>
        );
    }
}
import React from 'react';

export default class Movies extends React.Component {
  constructor(props){
    super();

    this.boardModel = props.boardModel;
    this.state = this.boardModel.data;
    this.boardModel.addScope({
      name: 'movies',
      scope: this
    });
   
  }

  render() {
    let movies = this.state.movies.map((s, i)=>{
        return <div key={i} value={s}>{s.Title}</div>;
    })
    return <ul>{movies}</ul>
  }
}




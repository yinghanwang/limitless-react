import React from 'react';
import brain from  '../services/brain.js';

export default class ScoreBoard extends React.Component {
  constructor(props){
    super();
    
    this.state = {
      sum: props.sum
    };
    brain.on('updatetotal', this.onUpdateSum, this);
  }

  onUpdateSum(sum) {
    let ts = Object.assign({}, this.state);
    ts.sum = sum;
    this.setState(ts);
  }
 
  render() {
    return (
      <div>
        {this.state.sum}
      </div>
    );
  }
}
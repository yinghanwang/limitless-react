// all data model could have proxy setup
// in order to communicate with data source
import http from 'axios';

// model
export default class BaseModel {
    constructor(config) {
        config = config || {};
        this.scope = [];
    }

    init() {
    };
    
    request(options) {
        return http[options.method || 'get'](options.url);
    }
    
    digest(scopeName) {
        let s = this.scope;
        if (s && s.length) {
            for (let i = 0; i < s.length; i++) {
                if(scopeName && s[i].name === scopeName) {
                    s[i].scope.setState(this.data);
                    return;
                } else {
                    s[i].scope.setState(this.data);
                }
            }
        }
    };

    addScope(s) {
        this.scope.push(s);
    };

    setScope(s) {
        this.scope = [s];
    };
    
    unsetScope(s) {
        this.scope.splice(this.scope.indexOf(s), 1);
    };


}

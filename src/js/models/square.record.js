import BaseModel from './base.model.js';
// model
class SquareRecord extends BaseModel{
    constructor(config) {
        super();
        config = config || {};
        this.scope = config.scope || null;

        this.data = {
            value: config.value
        };

        this.getValue = () => {
            return this.data.value;
        };

        this.setValue = (val) => {
            this.data.value = val;
        };

        this.doubleValue = () => {
            this.setValue(this.getValue() * 2);
            this.digest('square');
        };

        this.resetBackToOne = () => {
            this.setValue(1);
            this.digest('square');
        };
    }
}

export default SquareRecord;
import SquareModel from './square.record.js';
import BaseModel from './base.model.js';
// model

class BoardModel extends BaseModel{
    constructor(config) {
        super();
        config = config || {};
        this.data = {
            squares: [],
            sum: 0,
            movies: [],
            num: 10
        };

        this.init();
    }

    fetchBoardDetail() {
        return this.request({
            url: 'https://jsonmock.hackerrank.com/api/movies/search/'
        }).then((res)=>{
            this.data.movies = [...this.data.movies, ...res.data.data]; //this.movies.concat(res.data.data);
            this.digest('movies');f
        });
    }
    
    // override
    init() {
        this.initSquares();
    };
    
    initSquares() {
        for (let i = 0; i < this.data.num; i++) {
            this.data.squares.push(new SquareModel({
                value: i
            }));
        }
    };



    calTotal() {
        this.data.sum = 0;
        for (let i = 0; i < this.data.squares.length; i++) {
            this.data.sum = this.data.sum + this.data.squares[i].getValue();
        }
        
        this.digest('board');
        return this.data.sum;
    };
}

export default BoardModel;
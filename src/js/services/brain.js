export default {
	cache: {},

	trigger: function (topic, args) {
		if (!this.cache[topic]) return;
		for (var i = 0, el; el = this.cache[topic][i] ;i++) el.fn.apply(el.scope, args || []);
	},

	on: function (topic, callback, scope) {
		if (!this.cache[topic]) this.cache[topic] = [];
		this.cache[topic].push({ fn: callback, scope: scope || callback });
		return [topic, callback, scope];
	},

	off: function (handle) {
		var i = 0, el, topic = handle[0];
		if (!this.cache[topic]) return;
		for (; el = this.cache[topic][i]; i++) if (el.fn === handle[1]) this.cache[topic].splice(i, 1);
	}
}
